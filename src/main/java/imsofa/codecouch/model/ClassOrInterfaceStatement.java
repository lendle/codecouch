/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class ClassOrInterfaceStatement implements ExpProvider{
    private String modifier=null;
    private String name=null;
    private List<String> extendedTypes=new ArrayList<>();
    private List<String> implementedTypes=new ArrayList<>();
    private String comment=null;
    private List<FieldStatement> fields=new ArrayList<>();
    private List<MethodStatement> methods=new ArrayList<>();
    private List<ClassOrInterfaceStatement> innerClasses=new ArrayList<>();
    private List<ClassOrInterfaceStatement> innerInterfaces=new ArrayList<>();
    private boolean aClass=true;
    private boolean nested=false;
    private PackageStatement packageStatement=null;
    private List<ImportStatement> importStatements=new ArrayList<>();

    public PackageStatement getPackageStatement() {
        return packageStatement;
    }

    public void setPackageStatement(PackageStatement packageStatement) {
        this.packageStatement = packageStatement;
    }

    public List<ImportStatement> getImportStatements() {
        return importStatements;
    }

    public void setImportStatements(List<ImportStatement> importStatements) {
        this.importStatements = importStatements;
    }

    public boolean isNested() {
        return nested;
    }

    public void setNested(boolean nested) {
        this.nested = nested;
    }
    
    

    public boolean isaClass() {
        return aClass;
    }

    public void setaClass(boolean aClass) {
        this.aClass = aClass;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getExtendedTypes() {
        return extendedTypes;
    }

    public void setExtendedTypes(List<String> extendedTypes) {
        this.extendedTypes = extendedTypes;
    }

    public List<String> getImplementedTypes() {
        return implementedTypes;
    }

    public void setImplementedTypes(List<String> implementedTypes) {
        this.implementedTypes = implementedTypes;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<FieldStatement> getFields() {
        return fields;
    }

    public void setFields(List<FieldStatement> fields) {
        this.fields = fields;
    }

    public List<MethodStatement> getMethods() {
        return methods;
    }

    public void setMethods(List<MethodStatement> methods) {
        this.methods = methods;
    }

    public List<ClassOrInterfaceStatement> getInnerClasses() {
        return innerClasses;
    }

    public void setInnerClasses(List<ClassOrInterfaceStatement> innerClasses) {
        this.innerClasses = innerClasses;
    }

    public List<ClassOrInterfaceStatement> getInnerInterfaces() {
        return innerInterfaces;
    }

    public void setInnerInterfaces(List<ClassOrInterfaceStatement> innerInterfaces) {
        this.innerInterfaces = innerInterfaces;
    }
    
    public boolean hasQuestionDefined(){
        for(MethodStatement methodStatement : methods){
            if(methodStatement.isQuestion()){
                return true;
            }
        }
        for(ClassOrInterfaceStatement classOrInterfaceStatement : innerClasses){
            if(classOrInterfaceStatement.hasQuestionDefined()){
                return true;
            }
        }
        return false;
    }
    
    public String toString(){
        StringBuffer buffer=new StringBuffer();
        if(!nested){
            buffer.append(this.packageStatement);
            for(ImportStatement stmt : importStatements){
                buffer.append(stmt);
            }
        }
        buffer.append(this.modifier).append((aClass)?" class ":" interface ").append(name).append("{").append("\r\n");
        for(FieldStatement stmt : fields){
            buffer.append(stmt.toString()).append("\r\n");
        }
        for(MethodStatement stmt : methods){
            buffer.append(stmt.toString()).append("\r\n");
        }
        for(ClassOrInterfaceStatement stmt : innerInterfaces){
            buffer.append(stmt.toString()).append("\r\n");
        }
        for(ClassOrInterfaceStatement stmt : innerClasses){
            buffer.append(stmt.toString()).append("\r\n");
        }
        buffer.append("}");
        
        return buffer.toString();
    }

    @Override
    public String getExp() {
        return this.toString();
    }
}
