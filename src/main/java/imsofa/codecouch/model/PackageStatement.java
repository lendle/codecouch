/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.model;

/**
 *
 * @author lendle
 */
public class PackageStatement implements ExpProvider{
    private String exp=null;

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }
    
    public String toString(){
        return exp;
    }
}
