/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.renderer;

import imsofa.codecouch.gui.model.TreeFile;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author lendle
 */
public class FileTreeCellRenderer extends DefaultTreeCellRenderer{
    private ImageIcon folderWithQuestionIcon=new ImageIcon(this.getClass().getClassLoader().getResource("if_edit_2561427.png"));
    private ImageIcon folderWithoutQuestionIcon=new ImageIcon(this.getClass().getClassLoader().getResource("if_folder_2561441.png"));
    private ImageIcon normalFileIcon=new ImageIcon(this.getClass().getClassLoader().getResource("if_file_2561437.png"));
    private ImageIcon questionFileIcon=new ImageIcon(this.getClass().getClassLoader().getResource("if_file-text_2561436.png"));
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JLabel label=(JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus); //To change body of generated methods, choose Tools | Templates.
        TreeFile file=(TreeFile) value;
        label.setText(file.getName());
        if(file.isDirectory()){
            if(file.isQuestion()){
                label.setIcon(folderWithQuestionIcon);
            }else{
                label.setIcon(folderWithoutQuestionIcon);
            }
        }else{
            if(file.isQuestion()){
                label.setIcon(questionFileIcon);
            }else{
                label.setIcon(normalFileIcon);
            }
        }
        return label;
    }
    
}
