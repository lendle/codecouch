/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.renderer;

import imsofa.codecouch.model.ClassOrInterfaceStatement;
import imsofa.codecouch.model.FieldStatement;
import imsofa.codecouch.model.ImportStatement;
import imsofa.codecouch.model.MethodStatement;
import java.awt.Color;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author lendle
 */
public class QuestionContentTreeCellRenderer extends DefaultTreeCellRenderer {
    private ImageIcon importIcon=new ImageIcon(this.getClass().getClassLoader().getResource("import.png"));
    private ImageIcon fieldIcon=new ImageIcon(this.getClass().getClassLoader().getResource("field.png"));
    private ImageIcon methodIcon=new ImageIcon(this.getClass().getClassLoader().getResource("method.png"));
    private ImageIcon classIcon=new ImageIcon(this.getClass().getClassLoader().getResource("class.png"));
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JLabel label=(JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus); //To change body of generated methods, choose Tools | Templates.
        if(value instanceof ClassOrInterfaceStatement){
            label.setText(((ClassOrInterfaceStatement)value).getName());
            label.setIcon(classIcon);
        }else if(value instanceof FieldStatement){
            label.setText(((FieldStatement)value).getName());
            label.setIcon(fieldIcon);
        }else if(value instanceof MethodStatement){
            MethodStatement method=(MethodStatement) value;
            if(method.isQuestion()){
                label.setForeground(Color.RED);
            }
            label.setText(((MethodStatement)value).getDeclaration());
            label.setIcon(methodIcon);
        }else if(value instanceof ImportStatement){
            label.setIcon(importIcon);
        }
        
        return label;
    }
    
}
