/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.model;

import imsofa.codecouch.model.ClassOrInterfaceStatement;
import imsofa.codecouch.model.FieldStatement;
import imsofa.codecouch.model.ImportStatement;
import imsofa.codecouch.model.MethodStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author lendle
 */
public class QuestionContentModel implements TreeModel{
    private ClassOrInterfaceStatement root=null;
    private Map<String, List> childrenListMap=new HashMap<>();
    //private List children=new ArrayList();
    
    public QuestionContentModel(ClassOrInterfaceStatement root){
        this.root=root;
        this.parseClassOrInterfaceStatement(root);
    }
    
    private void parseClassOrInterfaceStatement(ClassOrInterfaceStatement root){
        List children=new ArrayList();
        childrenListMap.put(root.getName(), children);
        for(ImportStatement stmt : root.getImportStatements()){
            children.add(stmt);
        }
        
        for(FieldStatement stmt : root.getFields()){
            children.add(stmt);
        }
        
        for(MethodStatement stmt : root.getMethods()){
            children.add(stmt);
        }
        
        for(ClassOrInterfaceStatement stmt : root.getInnerInterfaces()){
            children.add(stmt);
            this.parseClassOrInterfaceStatement(stmt);
        }
        
        for(ClassOrInterfaceStatement stmt : root.getInnerClasses()){
            children.add(stmt);
            this.parseClassOrInterfaceStatement(stmt);
        }
    }
    
    @Override
    public Object getRoot() {
        return this.root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        ClassOrInterfaceStatement cls=(ClassOrInterfaceStatement) parent;
        List children=childrenListMap.get(cls.getName());
        return children.get(index);
    }

    @Override
    public int getChildCount(Object parent) {
        ClassOrInterfaceStatement cls=(ClassOrInterfaceStatement) parent;
        List children=childrenListMap.get(cls.getName());
        return children.size();
    }

    @Override
    public boolean isLeaf(Object node) {
        if(!(node instanceof ClassOrInterfaceStatement)){
            return true;
        }else{
            ClassOrInterfaceStatement cls=(ClassOrInterfaceStatement) node;
            List children=childrenListMap.get(cls.getName());
            return children==null || children.isEmpty();
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        ClassOrInterfaceStatement cls=(ClassOrInterfaceStatement) parent;
        List children=childrenListMap.get(cls.getName());
        return children.indexOf(child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        
    }
    
}
