/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.model;

import imsofa.codecouch.Constants;
import imsofa.codecouch.JavaSourceParser;
import imsofa.codecouch.model.ClassOrInterfaceStatement;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.codehaus.plexus.util.FileUtils;

/**
 *
 * @author lendle
 */
public class FileSystemModel implements TreeModel {

    private Map<String, TreeFile> treeFileMap = new HashMap<>();
    private TreeFile root;

    private Vector listeners = new Vector();

    public FileSystemModel(File rootDirectory) {
        root = new TreeFile(rootDirectory.getAbsolutePath());
        root.setRoot(true);
        try {
            treeFileMap.put(root.getCanonicalPath(), root);
            scan(root);
        } catch (IOException ex) {
            Logger.getLogger(FileSystemModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //test
    private void scan(File parentFolder) throws IOException {
        for (File file : parentFolder.listFiles()) {
            TreeFile tf = new TreeFile(parentFolder, file.getName());
            treeFileMap.put(file.getCanonicalPath(), tf);
            if (file.isFile() && file.getName().endsWith(".java")) {
                String content = FileUtils.fileRead(file, "utf-8");
                if (content.contains(Constants.QUESTION)) {
                    try (Reader reader = new StringReader(content)) {
                        ClassOrInterfaceStatement classOrInterfaceStatement = new JavaSourceParser().parse(reader);
                        if (classOrInterfaceStatement.hasQuestionDefined()) {
                            //then this java file contains at least a question
                            tf.setQuestion(true);
                            File parentFile = file.getParentFile();
                            while (parentFile != null) {
                                TreeFile tf1 = treeFileMap.get(parentFile.getCanonicalPath());
                                if (tf1 != null) {
                                    if (tf1.isRoot() || tf1.isQuestion()) {
                                        break;
                                    } else {
                                        tf1.setQuestion(true);
                                        parentFile = tf1.getParentFile();
                                    }
                                } else {
                                    break;
                                }
                            }
                            tf.setClassOrInterfaceStatement(classOrInterfaceStatement);
                        }
                    }
                }
            } else if (file.isDirectory()) {
                scan(file);
            }
        }
    }

    public Object getRoot() {
        return root;
    }

    public Object getChild(Object parent, int index) {
        File directory = (File) parent;
        File[] children = directory.listFiles();
        try {
            return treeFileMap.get(children[index].getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger(FileSystemModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getChildCount(Object parent) {
        File file = (File) parent;
        if (file.isDirectory()) {
            String[] fileList = file.list();
            if (fileList != null) {
                return file.list().length;
            }
        }
        return 0;
    }

    public boolean isLeaf(Object node) {
        File file = (File) node;
        return file.isFile();
    }

    public int getIndexOfChild(Object parent, Object child) {
        File directory = (File) parent;
        File file = (File) child;
        String[] children = directory.list();
        for (int i = 0; i < children.length; i++) {
            if (file.getName().equals(children[i])) {
                return i;
            }
        }
        return -1;

    }

    public void valueForPathChanged(TreePath path, Object value) {
        File oldFile = (File) path.getLastPathComponent();
        String fileParentPath = oldFile.getParent();
        String newFileName = (String) value;
        File targetFile = new File(fileParentPath, newFileName);
        oldFile.renameTo(targetFile);
        File parent = new File(fileParentPath);
        int[] changedChildrenIndices = {getIndexOfChild(parent, targetFile)};
        Object[] changedChildren = {targetFile};
        fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);

    }

    private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
        TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
        Iterator iterator = listeners.iterator();
        TreeModelListener listener = null;
        while (iterator.hasNext()) {
            listener = (TreeModelListener) iterator.next();
            listener.treeNodesChanged(event);
        }
    }

    public void addTreeModelListener(TreeModelListener listener) {
        listeners.add(listener);
    }

    public void removeTreeModelListener(TreeModelListener listener) {
        listeners.remove(listener);
    }

    class Test {

        //###question
        //###question_exp 123456
        public void test_test() {

        }
    }

    interface Test2 extends Cloneable {

    }

    static final class Test3 {

    }

}
