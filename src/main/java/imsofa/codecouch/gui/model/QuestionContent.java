/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.model;

import imsofa.codecouch.model.ExpProvider;
import imsofa.codecouch.model.FieldStatement;
import imsofa.codecouch.model.ImportStatement;
import imsofa.codecouch.model.MethodStatement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class QuestionContent {
    //exactly one of them
    private List<ImportStatement> importStatements=new ArrayList<>();
    private List<FieldStatement> fields=new ArrayList<>();
    private MethodStatement method=null;

    public List<ImportStatement> getImportStatements() {
        return importStatements;
    }

    public void setImportStatements(List<ImportStatement> importStatements) {
        this.importStatements = importStatements;
    }

    public List<FieldStatement> getFields() {
        return fields;
    }

    public void setFields(List<FieldStatement> fields) {
        this.fields = fields;
    }

    public MethodStatement getMethod() {
        return method;
    }

    public void setMethod(MethodStatement method) {
        this.method = method;
    }
    
    
}
