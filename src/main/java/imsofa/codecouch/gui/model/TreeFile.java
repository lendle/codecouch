/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch.gui.model;

import imsofa.codecouch.model.ClassOrInterfaceStatement;
import java.io.File;

/**
 *
 * @author lendle
 */
public class TreeFile extends File {
    
    private boolean question = false;
    private boolean root = false;
    /**
     * not null if the corresponding file (not directory) contains at least
     * one question
     */
    private ClassOrInterfaceStatement classOrInterfaceStatement=null;

    public ClassOrInterfaceStatement getClassOrInterfaceStatement() {
        return classOrInterfaceStatement;
    }

    public void setClassOrInterfaceStatement(ClassOrInterfaceStatement classOrInterfaceStatement) {
        this.classOrInterfaceStatement = classOrInterfaceStatement;
    }

    public boolean isRoot() {
        return root;
    }

    public void setRoot(boolean root) {
        this.root = root;
    }

    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public TreeFile(File parent, String child) {
        super(parent, child);
    }

    public TreeFile(String pathname) {
        super(pathname);
    }

    public String toString() {
        return getName();
    }
    
}
