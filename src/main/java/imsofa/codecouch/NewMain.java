/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.io.FileInputStream;
import java.io.Serializable;

/**
 *
 * @author lendle
 */
public class NewMain implements Serializable{
    @SuppressWarnings(value={"unchecked"})
    /**
     * @param args the command line arguments
     * -- 123
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
//        InvocationRequest request = new DefaultInvocationRequest();
//        request.setPomFile(new File("/home/lendle/dev/NetBeansProjects/LabUtils/CodingMate/pom.xml"));
//        request.setGoals(Collections.singletonList("exec:java -Dexec.mainClass=imsofa.codingmate.CodingMateUI"));
//        Invoker invoker = new DefaultInvoker();
//        invoker.execute(request);
        
        FileInputStream in = new FileInputStream("/home/lendle/dev/NetBeansProjects/LabUtils/CodeCouch/src/main/java/imsofa/codecouch/gui/model/FileSystemModel.java");
        CompilationUnit cu = JavaParser.parse(in);
        cu.accept(new VoidVisitorAdapter<Void>(){
            private void printComment(Comment n){
                //System.out.println(n.getContent());
            }
            @Override
            public void visit(ClassOrInterfaceDeclaration n, Void arg) {
                if(n.getComment().isPresent()){
                    //printComment(n.getComment().get());
                }
                System.out.println(n.getName()+":"+n.getModifiers());
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(ImportDeclaration n, Void arg) {
                System.out.println("import "+n.toString()+":"+n.getParentNode().get().getClass());
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }
            
            

            @Override
            public void visit(ConstructorDeclaration n, Void arg) {
                if(n.getComment().isPresent()){
                    printComment(n.getComment().get());
                }
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(FieldDeclaration n, Void arg) {
                if(n.getComment().isPresent()){
                    printComment(n.getComment().get());
                }
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }
           
            @Override
            public void visit(MethodDeclaration n, Void arg) {
                System.out.println("method: "+n+":"+((ClassOrInterfaceDeclaration)n.getParentNode().get()).isNestedType());
                if(n.getComment().isPresent()){
                    printComment(n.getComment().get());
                    /*if(n.getComment().get().isJavadocComment()){
                        JavadocComment comment=n.getComment().get().asJavadocComment();
                        Javadoc javadoc=comment.parse();
                        System.out.println(javadoc.getBlockTags());
                    }*/
                }else if(n.getAnnotations().size()>0){
                    //System.out.println(n.getAnnotation(0).getName());
                }
                //System.out.println(n.getComment());
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }
            
        }, null);
        ///home/lendle/dev/NetBeansProjects/LabUtils/CodingMate/src/main/java/imsofa/codingmate/CodingMateUI.java
    }

}
