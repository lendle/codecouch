/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codecouch;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import imsofa.codecouch.model.ClassOrInterfaceStatement;
import imsofa.codecouch.model.FieldStatement;
import imsofa.codecouch.model.ImportStatement;
import imsofa.codecouch.model.MethodStatement;
import imsofa.codecouch.model.PackageStatement;
import java.io.FileReader;
import java.io.Reader;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class JavaSourceParser {

    public ClassOrInterfaceStatement parse(Reader javaSource) {
        ClassOrInterfaceStatement cls = new ClassOrInterfaceStatement();
        Map<String, ClassOrInterfaceStatement> clsStatementMap = new HashMap<>();
        CompilationUnit cu = JavaParser.parse(javaSource);
        cu.accept(new VoidVisitorAdapter<Void>() {
            @Override
            public void visit(ImportDeclaration n, Void arg) {
                ImportStatement importStatement = new ImportStatement();
                importStatement.setExp(n.toString());
                cls.getImportStatements().add(importStatement);
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(PackageDeclaration n, Void arg) {
                PackageStatement stmt = new PackageStatement();
                stmt.setExp(n.toString());
                cls.setPackageStatement(stmt);
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(MethodDeclaration n, Void arg) {
                ClassOrInterfaceDeclaration parent = (ClassOrInterfaceDeclaration) (n.getParentNode().get());
                ClassOrInterfaceStatement _cls = clsStatementMap.get(parent.getName().toString());
                MethodStatement stmt = new MethodStatement();
                if(n.getComment().isPresent()){
                    Comment comment=n.getComment().get();
                    if(comment.toString().contains(Constants.QUESTION)){
                        stmt.setQuestion(true);
                    }
                }
                stmt.setDeclaration(n.getDeclarationAsString());
                stmt.setExp(n.toString());
                stmt.setName(n.getNameAsString());
                _cls.getMethods().add(stmt);
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(FieldDeclaration n, Void arg) {
                ClassOrInterfaceDeclaration parent = (ClassOrInterfaceDeclaration) (n.getParentNode().get());
                ClassOrInterfaceStatement _cls = clsStatementMap.get(parent.getName().toString());
                FieldStatement stmt = new FieldStatement();
                stmt.setName(n.getVariable(0).getNameAsString());
                stmt.setExp(n.toString());
                _cls.getFields().add(stmt);
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void visit(ClassOrInterfaceDeclaration n, Void arg) {
                if (n.isNestedType() == false) {
                    clsStatementMap.put(n.getName().toString(), cls);
                    cls.setName(n.getName().toString());
                    cls.setModifier(convertModifiers2String(n.getModifiers()));
                    cls.setaClass(true);
                } else {
                    ClassOrInterfaceStatement nestedClassOrInterfaceStatement = new ClassOrInterfaceStatement();
                    clsStatementMap.put(n.getName().toString(), nestedClassOrInterfaceStatement);
                    nestedClassOrInterfaceStatement.setNested(true);
                    nestedClassOrInterfaceStatement.setName(n.getName().toString());
                    nestedClassOrInterfaceStatement.setModifier(convertModifiers2String(n.getModifiers()));
                    if (n.isInterface()) {
                        cls.getInnerInterfaces().add(nestedClassOrInterfaceStatement);
                        nestedClassOrInterfaceStatement.setaClass(false);
                    } else {
                        cls.getInnerClasses().add(nestedClassOrInterfaceStatement);
                        nestedClassOrInterfaceStatement.setaClass(true);
                    }
                }
                super.visit(n, arg); //To change body of generated methods, choose Tools | Templates.
            }
            
            private String convertModifiers2String(EnumSet<Modifier> en){
                StringBuffer buffer=new StringBuffer();
                Modifier [] modifiers=en.toArray(new Modifier[0]);
                for(int i=0; modifiers!=null && i<modifiers.length; i++){
                    if(buffer.length()>0){
                        buffer.append(" ");
                    }
                    buffer.append(modifiers[i].asString().toLowerCase());
                }
                return buffer.toString();
            }
        }, null);
        return cls;
    }

    public static void main(String[] args) throws Exception {
        JavaSourceParser parser = new JavaSourceParser();
        try (Reader reader = new FileReader("/home/lendle/dev/NetBeansProjects/LabUtils/CodeCouch/src/main/java/imsofa/codecouch/gui/model/FileSystemModel.java")) {
            ClassOrInterfaceStatement cls=parser.parse(reader);
            System.out.println(cls);
        }
    }
}
